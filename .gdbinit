# Turn off confirmations and verbosity
set confirm off
set verbose off

# Have GDB detect language from file extension
set language auto

# Printing options
set print array on
set print elements 1000
set print null-stop
set print pretty on
set print demangle on
set print asm-demangle on
set print static-members on
set print vtbl on
set print object on
set print union on

# STL container pretty print macros (pre GDB 7.x and pretty printers)
source ~/.gdb/dbinit_stl_views-1.03.txt
