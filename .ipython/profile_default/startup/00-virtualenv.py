import site
import sys
import os

# Detect virtualenv
if 'VIRTUAL_ENV' in os.environ:

    virtualenv_root = os.environ['VIRTUAL_ENV']

    virtualenv = os.path.join(virtualenv_root, 'lib',
                       'python%d.%d' % sys.version_info[:2],
                       'site-packages')
    site.addsitedir(virtualenv)

    print ('\nVirtual Environment: %s' % virtualenv_root)
    
    del virtualenv, virtualenv_root
del site, sys, os