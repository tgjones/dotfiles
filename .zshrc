# Source profile
source ~/.zprofile

# oh-my-zsh
export ZSH=$HOME/.oh-my-zsh
ZSH_THEME="tgjones"
plugins=(git docker python)
source $ZSH/oh-my-zsh.sh

# History settings
HISTFILE=$HOME/.zsh_history

# Custom key bindings
bindkey -v
bindkey '^R' history-incremental-search-backward

# Enable direnv
if which direnv > /dev/null 2>&1; then
    eval "$(direnv hook zsh)"
    export DIRENV_LOG_FORMAT=
fi
