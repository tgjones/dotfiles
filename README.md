TJ's Dotfiles
=============

This repository contains the current state of my dotfiles, and is used mostly in anger on Ubuntu 16.04 but should work reasonably well on most *nix style operating systems.

Installation
------------

Run the `install.sh` script, this copies all the dotfiles across to your home directory by default, backing up any that are found to already exist.  Use the `-f` switch to stop the backups, and the `-l` switch to symbolicly link to the repository instead.

### Bootstrap Script

Additionally, by specifying the `-b` switch, `install.sh` will run `bootstrap.sh` before the install, which downloads and installs various pre-requisite packages and plugins.

Windows Specific
----------------

There are a few additional Windows specific AutoHotkey and PowerShell scripts and configuration files in the `windows` sub-directory that are not part of the normal installation, and require manual installation.