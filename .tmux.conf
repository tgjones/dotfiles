# Set prefix to Ctrl-A
unbind-key C-b
set -g prefix C-a

# Start numbers from 1
set -g base-index 1
setw -g pane-base-index 1

# Scrollback buffer
set -g history-limit 5000

# Statusline messages last longer
set -g display-time 3000

# Vim style controls
setw -g mode-keys vi
setw -g status-keys vi

# Vim style pane management
bind v split-window -h
bind s split-window -v
bind h select-pane -L
bind j select-pane -D
bind k select-pane -U
bind l select-pane -R

# Vim style shell execution
bind ! command-prompt -p "Run:" "run-shell '%%'"

# Window/session management
bind S choose-session
bind , command-prompt -p "Rename window:" "rename-window '%%'"
bind . command-prompt -p "Rename session:" "rename-session '%%'"

# Turn mouse on everywhere
setw -g mode-mouse on
set -g mouse-resize-pane on
set -g mouse-select-pane on
set -g mouse-select-window on

# Activity/bell settings
setw -g monitor-activity on
set -g visual-activity on
set -g window-status-activity-attr dim,underscore
set -g window-status-bell-attr dim,underscore

# Terminal and window titles
set -g set-titles on
set -g set-titles-string "[#S] #(whoami)@#h #I:#W"
setw -g automatic-rename on

# Enable aggressive pane resizing
setw -g aggressive-resize on

# Status bar settings
set -g status-position bottom
set -g status-interval 5
set -g status-attr dim
set -g status-justify centre
set -g status-bg blue
set -g status-fg white
set -g status-left '[#S] #(whoami)@#h'
set -g status-left-attr dim
set -g status-left-length 30
set -g status-right '%d %b %H:%M'
set -g status-right-attr dim

# Pane border colours
set -g pane-active-border-fg blue
set -g pane-active-border-bg black
set -g pane-border-fg white
set -g pane-border-bg black

# Window title settings
setw -g window-status-format "#I:#W"
setw -g window-status-current-format "#I:#W"
setw -g window-status-current-attr bright

# Reload config bound to r
bind r source-file ~/.tmux.conf \; display-message "Reloaded tmux configuration..."

# Create default session
new-session
