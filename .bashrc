#!/usr/bin/env bash

# GENERAL INIT
# ------------

# Default path
export PATH=/sbin:/usr/sbin:$PATH

# Local path
if [ -d ~/.local/bin ]; then
    export PATH=~/.local/bin:$PATH
fi

# Default text editor is vi
if command -v vim >/dev/null; then
    export EDITOR=vim
    alias vi=vim
else
    export EDITOR=vi
fi
export VIEWER=$EDITOR

# Enable X11 forwarding for su/sudo
export XAUTHORITY=~/.Xauthority

# TERMINAL TWEAKS
# ---------------

# Update LINES and COLUMNS after each command
shopt -s checkwinsize

# Set title in either xterm or screen
if [[ $TERM == xterm* || $TERM == rxvt* ]]; then
    function title () { echo -ne "\033]0;$1\007"; }
    export PROMPT_COMMAND="title $USER@$HOSTNAME; $PROMPT_COMMAND"
elif [[ $TERM == screen* ]]; then
    function title () { echo -ne "\033k\033\0134\033k$1\033\0134"; }
    export PROMPT_COMMAND="title $USER@$HOSTNAME; $PROMPT_COMMAND"
fi

# BASH HISTORY
# ------------

shopt -s histappend
export HISTFILE=~/.bash_history
export HISTCONTROL=ignoredups:erasedups
export HISTFILESIZE=10000
export HISTSIZE=100000
export HISTIGNORE="exit"

# BASH PROMPT
# -----------

ps1_k8s_context() {
    kubectl config current-context | sed -e 's/\(.*\)/ (k8s:\1)/'
}

ps1_git_branch() {
    git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/ (git:\1)/'
}

ps1_exit_code()
{
    echo -n ${?#0} | sed -e 's/\(.*\)/ (exit:\1)/'
}

export PS1="\[\033[32m\]\w\[\033[33m\]\$(ps1_git_branch)\$(ps1_k8s_context)\[\033[00m\]\[\e[31m\]\$(ps1_exit_code)\[\e[m\]\n\u@\h $ "

# PLATFORM SPECIFIC
# -----------------

# macOS specific
if [[ "$OSTYPE" == "darwin"* ]]; then

    # Enable iTerm2 shell integration
    [ -f ~/.iterm2_shell_integration.bash ] && . ~/.iterm2_shell_integration.bash

    # HomeBrew specific
    if command -v brew > /dev/null; then
        export PATH=$(brew --prefix)/bin:$PATH
        export CPATH=$(brew --prefix)/include:$CPATH
        export LIBRARY_PATH=$(brew --prefix)/lib:$LIBRARY_PATH
        export HOMEBREW_GITHUB_API_TOKEN="c3e0d0a7400c6ab7fa8be4308ce7ad6cf3ff9308"
        [ -f $(brew --prefix)/etc/bash_completion ] && . $(brew --prefix)/etc/bash_completion
    fi

    export DYLD_LIBRARY_PATH=$LIBRARY_PATH:$DYLD_LIBRARY_PATH

# Non macOS systems
else

    # Shared settings on all non-macOS
    export PATH=/usr/local/bin:$PATH
    export CPATH=/usr/local/include:$CPATH
    export LIBRARY_PATH=/usr/local/lib:$LIBRARY_PATH
    export LD_LIBRARY_PATH=$LIBRARY_PATH:$LD_LIBRARY_PATH

    # Linux specific
    if [[ "$OSTYPE" == "linux-gnu" ]]; then

        # Use 32-bit mode in Wine
        export WINEARCH=win32
        export WINEPREFIX=~/.wine/win32

    fi
fi

# Bash on Ubuntu on Windows
if grep -q Microsoft /proc/version 2> /dev/null; then

    # File creation mask
    umask 0022

    # Docker
    export DOCKER_HOST=tcp://0.0.0.0:2375

fi

# BASH COMPLETION
# ---------------

# Enable completion in sudo
complete -cf sudo

# Filenames to ignore
export FIGNORE="CVS:.svn:.git"

# Completion scripts
[ -f /etc/bash_completion ] && . /etc/bash_completion
[ -f ~/.git-completion.bash ] && . ~/.git-completion.bash
command -v aws_completer > /dev/null && complete -D '$(which aws_completer)' aws
command -v kubectl > /dev/null && . <(kubectl completion bash)
command -v helm > /dev/null && . <(helm completion bash)

# LANGUAGE SPECIFIC
# -----------------

# Python
export PIPENV_VENV_IN_PROJECT=1
export PIPENV_SHELL_FANCY=1
[ -f ~/.pythonrc ] && export PYTHONSTARTUP=~/.pythonrc

# Node
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh"
[ -s "$NVM_DIR/bash_completion" ] && . "$NVM_DIR/bash_completion"

# Java
export JAVA_HOME="$(/usr/libexec/java_home)"
command -v jenv > /dev/null && eval "$(jenv init -)"

# Go
export GOPATH=~/.local/go
export PATH=$PATH:$GOPATH/bin

# AWS CONFIG
# ----------

if [ -f ~/.aws/credentials ]; then
    export AWS_ACCESS_KEY_ID=$(cat ~/.aws/credentials | grep aws_access_key_id | cut -d ' ' -f 3)
    export AWS_SECRET_ACCESS_KEY=$(cat ~/.aws/credentials | grep aws_secret_access_key | cut -d ' ' -f 3)
fi

# AWS configuration
if [ -f ~/.aws/config ]; then
    export AWS_DEFAULT_REGION=$(cat ~/.aws/config | grep region | cut -d ' ' -f 3)
fi

# MISC UTILS
# ----------

# direnv
if command -v direnv > /dev/null; then
    eval "$(direnv hook bash)"
    export DIRENV_LOG_FORMAT=
fi

# ALIASES
# -------

# Global aliases
alias grep='grep --color=auto'
alias fgrep='fgrep --color=auto'
alias egrep='egrep --color=auto'
alias xclip='xclip -selection clipboard'
alias df='df -h'
alias du='du -h'
alias k='kubectl'

if [[ "$OSTYPE" == darwin* ]]; then
    alias ls='ls -G'
    alias la='ls -A -G'
    alias ll='ls -lF -G'
    alias lla='ls -lFA -G'
    alias l='ls -CF -G'
else
    alias ls='ls -h --color=auto --group-directories-first'
    alias la='ls -hA --color=auto --group-directories-first'
    alias ll='ls -hlF --color=auto --group-directories-first'
    alias lla='ls -hlFA --color=auto --group-directories-first'
    alias l='ls -hCF --color=auto --group-directories-first'
fi
