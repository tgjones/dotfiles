# PyEnv environment
local pyenv_version=''
if which pyenv > /dev/null 2>&1; then
    pyenv_version='‹python:$(pyenv version | sed -e "s/ (set.*$//")›'
fi

# Git settings
local git_branch='$(git_prompt_info)'
ZSH_THEME_GIT_PROMPT_PREFIX="‹git:"
ZSH_THEME_GIT_PROMPT_SUFFIX="›"

# Actual prompt
PROMPT="%n@%m:%~ ${pyenv_version} ${git_branch}
λ "
RPROMPT="$(date +%T)"
